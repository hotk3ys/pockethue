import serial
import requests
import json
import time

serie = serial.Serial('COM3', 9600)
USER = 'FxTl7AGR9exYijKuW--AzvE4ZFIOauQ8ltneB0yW'
HOST = '10.0.1.22'

oldRaw = serie.readline()
oldPot = 0
oldBri = 0

while True:
    raw = serie.readline()
    if oldRaw != raw:
        oldRaw = raw
        values = raw.split(',')
        ldr = values[0].strip()[1:]
        pot = values[1].strip()[1:]
        button = values[2].strip()[1:]
        if button != '0':
            if button == '3':
                light = '3'
            elif button == '4':
                light = '1'
            elif button == '5':
                light = '2'
            url = 'http://' + HOST + '/api/' + USER + '/lights/' + light
            headers = {"Content-Type": "application/json"}
            response = requests.get(url, headers=headers)
            state = json.loads(response.text)
            state = state['state']
            if state['on']:
                data = { "on": False }
            else:
                data = { "on": True }
            url = 'http://' + HOST + '/api/' + USER + '/lights/' + light + '/state'
            headers = {"Content-Type": "application/json"}
            response = requests.put(url, data=json.dumps(data), headers=headers)
            time.sleep(0.5)

        # HUE
        if pot != oldPot:
            oldPot = pot
            pot = int(pot)
            data = { "hue": pot }
            url = 'http://' + HOST + '/api/' + USER + '/groups/1/action'
            headers = {"Content-Type": "application/json"}
            response = requests.put(url, data=json.dumps(data), headers=headers)
            print(response.text)
            time.sleep(0.5)
