const int b1 = 2;
const int b2 = 3;
const int b3 = 4;
const int b4 = 5;
const int b5 = 6;

// Analog
const int pot = 0;
const int ldr = 1;

// Variables
int ldrValue, potValue;
int b1Val, b2Val, b3Val, b4Val, b5Val;
int oldPot, oldLdr;
int oldB1, oldB2, oldB3, oldB4, oldB5;
unsigned int hue, bri;

void setup() {
  Serial.begin(9600);
  pinMode(b1, INPUT_PULLUP);
  pinMode(b2, INPUT_PULLUP);
  pinMode(b3, INPUT_PULLUP);
  pinMode(b4, INPUT_PULLUP);
  pinMode(b5, INPUT_PULLUP);
}

void loop() {
  ldrValue = analogRead(ldr);
  potValue = analogRead(pot);
  hue = map(potValue, 0, 1023, 0, 65535);
  bri = map(ldrValue, 0, 1023, 254, 0);
  b1Val = digitalRead(b1);
  b2Val = digitalRead(b2);
  b3Val = digitalRead(b3);
  b4Val = digitalRead(b4);
  b5Val = digitalRead(b5);  

  Serial.print("L");
  //Serial.print(ldrValue); 
  Serial.print(bri);
  Serial.print(",P");
  //Serial.print(potValue);
  Serial.print(hue);

  if(b1Val == LOW) Serial.println(",B1");
  else if(b2Val == LOW) Serial.println(",B2");
  else if(b3Val == LOW) Serial.println(",B3");
  else if(b4Val == LOW) Serial.println(",B4");
  else if(b5Val == LOW) Serial.println(",B5");
  else Serial.println(",B0");

  delay(250);
}
